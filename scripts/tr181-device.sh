#!/bin/sh

. /usr/lib/amx/scripts/amx_init_functions.sh

name="tr181-device"
datamodel_root="Device"

case $1 in
    boot)
        process_boot ${name} -D
        ;;
    start)
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name} 20
        ;;
    shutdown)
        process_shutdown ${name}
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        process_debug_info ${datamodel_root} true 1
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|restart|debuginfo]"
        ;;
esac
