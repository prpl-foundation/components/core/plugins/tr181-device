/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__DEVICE_DIAGNOSTICS_H__)
#define __DEVICE_DIAGNOSTICS_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_function.h>
#include <amxo/amxo.h>
#include <amxm/amxm.h>

#define DEBUG_INFO_CMD             "/bin/getDebugInformation"
#define DEBUG_FILEPATH             "/tmp/debug_all.tar.gz"

//getDeubugInformation Options
#define DEBUG_INFO_KEY_ALL         "All"
#define DEBUG_INFO_KEY_NETWORK     "Network"
#define DEBUG_INFO_KEY_WIFI        "Wifi"
#define DEBUG_INFO_KEY_PROCESS     "Process"
#define DEBUG_INFO_KEY_MEMORY      "Memory"
#define DEBUG_INFO_KEY_LOG         "Log"
#define DEBUG_INFO_KEY_SERVICE     "Service"
#define DEBUG_INFO_KEY_CORE        "Core"
#define DEBUG_INFO_KEY_OUTPUT      "Output"
#define DEBUG_INFO_KEY_FILEPATH    "FilePath"

//Sub Process Info
#define DEBUG_RET_KEY_PROC_PID            "ID"
#define DEBUG_RET_KEY_EXIT_CODE             "ExitCode"

#define STDOUT_BUFFER_SIZE   128

#define MOD_DEBUG_INFO_CTRL "debug-info"
#define MOD_DEBUG_INFO_NAME "mod-debug-info"

#define DEBUG_INFO_STATUS_KEY "Status"
#define DEBUG_INFO_RESULTS_KEY "Results"

#define DEBUG_INFO_STATUS_COMPLETE          "Complete"
#define DEBUG_INFO_STATUS_ERROR_INT         "Error_Internal"
#define DEBUG_INFO_STATUS_ERROR_OTHER       "Error_Other"
#define DEBUG_INFO_STATUS_NOT_COMPLETE      "Not_Complete"

typedef struct _debug_info_scheduler {
    uint64_t call_id;
    int32_t pid;
    amxc_string_t* file_name;
} debug_info_scheduler_t;

typedef enum _debug_info_status_ {
    complete,
    error_other,
    error_internal,
    not_complete,
    debug_info_num_of_status
} debug_info_status;

amxd_status_t debug_info_start(void (* check_function)(const char* const,
                                                       const amxc_var_t* const,
                                                       void* const),
                               debug_info_scheduler_t* sch,
                               amxc_var_t* ret);
amxd_status_t debug_info_stop(debug_info_scheduler_t* sch);
void debug_info_sched_check(UNUSED const char* const event_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv);

#ifdef __cplusplus
}
#endif

#endif // __DEVICE_DIAGNOSTICS_H__
