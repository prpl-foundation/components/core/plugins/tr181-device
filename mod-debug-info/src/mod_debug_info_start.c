/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>
#include <amxp/amxp_slot.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "device_diagnostics.h"
#include "mod_debug_info.h"

#define STRING_IS_NULL(x)   ((x) == NULL) || (strcmp(x, "(null)") == 0)
// module trace zone
#define ME "mod-debug"

static void proc_cmd_array_it_free(amxc_array_it_t* it) {
    free(it->data);
}

/**
 * @brief
 * Builds the command for launching the getDebugInformation tool
 *
 * @param params Input parameters
 * @param cmd Output command of the debug information generation
 */
static int create_get_debug_command(amxc_var_t* params, amxc_array_t* cmd) {
    int rv = -1;
    const char* param_string = NULL;
    bool param_bool = false;

    when_null_trace(params, exit, ERROR, "Input parameter is NULL.");

    amxc_array_init(cmd, 3);

    //getDebugInformation tool
    amxc_array_append_data(cmd, strdup(DEBUG_INFO_CMD));

    //option --all
    param_string = GET_CHAR(params, DEBUG_INFO_KEY_ALL);
    if(param_string != NULL) {
        amxc_array_append_data(cmd, strdup("-a"));
        amxc_array_append_data(cmd, strdup(param_string));
        param_string = NULL;
    }
    //option --network
    param_bool = GET_BOOL(params, DEBUG_INFO_KEY_NETWORK);
    if(param_bool == true) {
        amxc_array_append_data(cmd, strdup("-n"));
    }
    //option --wifi
    param_bool = GET_BOOL(params, DEBUG_INFO_KEY_WIFI);
    if(param_bool == true) {
        amxc_array_append_data(cmd, strdup("-w"));
    }
    //option --process
    param_bool = GET_BOOL(params, DEBUG_INFO_KEY_PROCESS);
    if(param_bool == true) {
        amxc_array_append_data(cmd, strdup("-p"));
    }
    //option --memory
    param_bool = GET_BOOL(params, DEBUG_INFO_KEY_MEMORY);
    if(param_bool == true) {
        amxc_array_append_data(cmd, strdup("-m"));
    }
    //option --log
    param_bool = GET_BOOL(params, DEBUG_INFO_KEY_LOG);
    if(param_bool == true) {
        amxc_array_append_data(cmd, strdup("-l"));
    }
    //option --service
    param_bool = GET_BOOL(params, DEBUG_INFO_KEY_SERVICE);
    if(param_bool == true) {
        amxc_array_append_data(cmd, strdup("-s"));
    }
    //option --core
    param_string = GET_CHAR(params, DEBUG_INFO_KEY_CORE);
    if(param_string != NULL) {
        amxc_array_append_data(cmd, strdup("-c"));
        amxc_array_append_data(cmd, strdup(param_string));
        param_string = NULL;
    }
    //option --output
    param_string = GET_CHAR(params, DEBUG_INFO_KEY_OUTPUT);
    if(param_string != NULL) {
        amxc_array_append_data(cmd, strdup("-o"));
        amxc_array_append_data(cmd, strdup(param_string));
        param_string = NULL;
    }
    rv = 0;

exit:
    return rv;
}

/**
 * @brief
 * Starts the getDebugInformation tool process
 *
 * @param function_name
 * @param params Parameters passed to the getDebugInformation tool
 * @param ret PID of getDebugInformation tool process
 * @return int
 */
int mod_debug_info_start(UNUSED const char* function_name,
                         amxc_var_t* params,
                         amxc_var_t* ret) {
    int rv = -1;
    amxp_subproc_t* proc_getdebug = NULL;
    int32_t pid = -1;
    amxc_array_t proc_cmd;

    rv = create_get_debug_command(params, &proc_cmd);
    when_failed_trace(rv, exit, ERROR, "Could not create the getDebugInformation command");

    rv = amxp_subproc_new(&proc_getdebug);
    when_failed_trace(rv, exit, ERROR, "Failed to initialize the subprocess.");

    rv = amxp_subproc_astart(proc_getdebug, &proc_cmd);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to start getDebugInfo process.");
        amxp_subproc_delete(&proc_getdebug);
        goto exit;
    }

    pid = amxp_subproc_get_pid(proc_getdebug);
    amxc_var_add_key(int32_t, ret, DEBUG_RET_KEY_PROC_PID, pid);


exit:
    amxc_array_clean(&proc_cmd, proc_cmd_array_it_free);
    return rv;
}
