/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "device_diagnostics.h"
#include "mod_debug_info.h"

// module trace zone
#define ME "mod-debug"

static int kill_debug_proccess(void) {
    int rv = 0;
    amxc_string_t* aux_string = NULL;
    amxc_string_new(&aux_string, 0);

    amxc_string_setf(aux_string, "pkill -9 -f \"^/bin/sh %s\"", DEBUG_INFO_CMD);
    rv = system(amxc_string_get(aux_string, 0));

    amxc_string_delete(&aux_string);
    return rv;
}


/**
 * @brief
 * Stops the getDebugInformation tool process
 *
 * @param function_name Unused parameter
 * @param params Input parameters for the test to stop
 * @param ret return params of the stopped test
 * @return int
 */
int mod_debug_info_stop(UNUSED const char* function_name,
                        amxc_var_t* params,
                        amxc_var_t* ret) {

    amxp_subproc_t* proc_getdebug = NULL;
    const int32_t pid = GET_INT32(params, DEBUG_RET_KEY_PROC_PID);
    int status = -1;

    when_true_trace(pid == 0, exit, ERROR, "Process id not found");

    proc_getdebug = amxp_subproc_find(pid);

    //Kill the process
    amxp_subproc_kill(proc_getdebug, SIGKILL);
    amxp_subproc_wait(proc_getdebug, 5);

    kill_debug_proccess();
    amxp_subproc_delete(&proc_getdebug);

    amxc_var_add_key(cstring_t, ret, DEBUG_INFO_RESULTS_KEY, "-");
    amxc_var_add_key(cstring_t, ret, DEBUG_INFO_STATUS_KEY, DEBUG_INFO_STATUS_NOT_COMPLETE);
    status = 0;

exit:
    return status;
}

