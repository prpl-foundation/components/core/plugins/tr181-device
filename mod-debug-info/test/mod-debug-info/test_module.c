/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxb/amxb_types.h>

#include <amxo/amxo.h>
#include <amxm/amxm.h>

#include "test_module.h"
#include "mod_debug_info.h"
#include "device_diagnostics.h"
#include "../common/mock.h"

#define TEST_DEBUG_FILE  "test_debug_file"
#define CREATE_DEBUG_FILE "touch " TEST_DEBUG_FILE
#define REMOVE_DEBUG_FILE "rm " TEST_DEBUG_FILE

static amxd_dm_t dm;
static amxo_parser_t parser;

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    handle_events();
    return 0;
}

int test_teardown(UNUSED void** state) {
    amxm_close_all();

    amxo_resolver_import_close_all();
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_can_load_module(UNUSED void** state) {
    amxm_shared_object_t* so = NULL;
    assert_int_equal(amxm_so_open(&so, "target_module", "../modules_under_test/target_module.so"), 0);
    handle_events();
}

void test_can_start_tool(UNUSED void** state) {
    amxc_var_t param;
    amxc_var_t ret;

    amxc_var_init(&param);
    amxc_var_init(&ret);

    amxc_var_set_type(&param, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &param, DEBUG_INFO_KEY_ALL, TEST_DEBUG_FILE);
    amxc_var_add_key(bool, &param, DEBUG_INFO_KEY_NETWORK, true);
    amxc_var_add_key(bool, &param, DEBUG_INFO_KEY_LOG, true);
    amxc_var_add_key(bool, &param, DEBUG_INFO_KEY_MEMORY, true);
    amxc_var_add_key(bool, &param, DEBUG_INFO_KEY_PROCESS, true);
    amxc_var_add_key(bool, &param, DEBUG_INFO_KEY_SERVICE, true);
    amxc_var_add_key(bool, &param, DEBUG_INFO_KEY_PROCESS, true);
    amxc_var_add_key(bool, &param, DEBUG_INFO_KEY_WIFI, true);

    assert_int_equal(mod_debug_info_start(NULL, &param, &ret), 0);

    amxc_var_clean(&param);
    amxc_var_clean(&ret);

}

void test_can_check_tool(UNUSED void** state) {
    amxc_var_t param;
    amxc_var_t ret;

    amxc_var_init(&param);
    amxc_var_init(&ret);

    amxc_var_set_type(&param, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(int32_t, &param, DEBUG_RET_KEY_PROC_PID, 10);
    amxc_var_add_key(cstring_t, &param, DEBUG_INFO_KEY_FILEPATH, TEST_DEBUG_FILE);

    //subproc return error
    set_subproc_find_status(success);
    set_subproc_wait_status(int_error);
    assert_int_equal(mod_debug_info_check(NULL, &param, &ret), error_internal);

    //subprocess finish successfully
    set_subproc_wait_status(stop);
    assert_int_equal(0, system(CREATE_DEBUG_FILE));
    assert_int_equal(mod_debug_info_check(NULL, &param, &ret), complete);
    assert_int_equal(0, system(REMOVE_DEBUG_FILE));

    set_subproc_find_status(real);
    amxc_var_clean(&param);
    amxc_var_clean(&ret);

}

void test_can_stop_tool(UNUSED void** state) {
    amxc_var_t param;
    amxc_var_t ret;

    amxc_var_init(&param);
    amxc_var_init(&ret);

    amxc_var_set_type(&param, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &param, DEBUG_INFO_KEY_ALL, TEST_DEBUG_FILE);
    assert_int_equal(mod_debug_info_start(NULL, &param, &ret), 0);

    amxc_var_clean(&param);
    amxc_var_clean(&ret);
    amxc_var_set_type(&param, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(int32_t, &param, DEBUG_RET_KEY_PROC_PID, 99);

    set_subproc_find_status(success);
    set_subproc_wait_status(running);
    assert_int_equal(mod_debug_info_stop(NULL, &param, &ret), 0);

    set_subproc_find_status(real);
    amxc_var_clean(&param);
    amxc_var_clean(&ret);

}




