/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include "device.h"
#include "device_interface_stack.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <stdio.h>

/**********************************************************
* Macro definitions
**********************************************************/

#define INTERFACE_STACK_OBJECT "Device.InterfaceStack."

//parameters in the netmodel event
#define DEVICE_INTERFACE_STACK_EVENT_PARAM_HIGHER_LAYER "HigherLayer"
#define DEVICE_INTERFACE_STACK_EVENT_PARAM_LOWER_LAYER "LowerLayer"
#define DEVICE_INTERFACE_STACK_EVENT_PARAM_HIGHER_ALIAS "HigherAlias"
#define DEVICE_INTERFACE_STACK_EVENT_PARAM_LOWER_ALIAS "LowerAlias"

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

static void device_handle_interface_stack_event_link_cb(const char* const sig_name,
                                                        const amxc_var_t* const data,
                                                        void* const priv);
static void device_handle_interface_stack_event_unlink_cb(const char* const sig_name,
                                                          const amxc_var_t* const data,
                                                          void* const priv);

/**********************************************************
* Functions
**********************************************************/

int device_subscribe_interface_stack(void) {
    int ret = -1;
    amxb_bus_ctx_t* netmodel_ctx = NULL;
    amxc_var_t ret_table;

    amxc_var_init(&ret_table);

    netmodel_ctx = amxb_be_who_has("NetModel.");
    when_null_trace(netmodel_ctx, exit, ERROR, "Could not find the context of netmodel");

    //subscribe to the link events
    ret = amxb_subscribe(netmodel_ctx, "NetModel.", "notification == 'nm:link'", device_handle_interface_stack_event_link_cb, NULL);
    when_failed_trace(ret, exit, ERROR, "Failed to subscribe on netmodel, exit error: %i", ret);

    //subscribe to the unlink events
    ret = amxb_subscribe(netmodel_ctx, "NetModel.", "notification == 'nm:unlink'", device_handle_interface_stack_event_unlink_cb, NULL);
    when_failed_trace(ret, exit, ERROR, "Failed to subscribe on netmodel, exit error: %i", ret);

    ret = amxb_call(netmodel_ctx, "NetModel.", "getLinkInformation", NULL, &ret_table, device_get_timeout());
    when_failed_trace(ret, exit, ERROR, "Failed to get the links from netmodel on start, exit error: %i", ret);
    amxc_var_for_each(it, GET_ARG(&ret_table, "0")) {
        device_handle_interface_stack_event_link_cb(NULL, it, NULL);
    }
exit:
    amxc_var_clean(&ret_table);
    return ret;
}

static void device_handle_interface_stack_event_link_cb(UNUSED const char* const sig_name,
                                                        const amxc_var_t* const data,
                                                        UNUSED void* const priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    const cstring_t higher_layer = NULL;
    const cstring_t lower_layer = NULL;
    const cstring_t higher_alias = NULL;
    const cstring_t lower_alias = NULL;
    amxd_object_t* inst = NULL;

    when_null(data, exit);

    higher_layer = GET_CHAR(data, DEVICE_INTERFACE_STACK_EVENT_PARAM_HIGHER_LAYER);
    lower_layer = GET_CHAR(data, DEVICE_INTERFACE_STACK_EVENT_PARAM_LOWER_LAYER);
    higher_alias = GET_CHAR(data, DEVICE_INTERFACE_STACK_EVENT_PARAM_HIGHER_ALIAS);
    lower_alias = GET_CHAR(data, DEVICE_INTERFACE_STACK_EVENT_PARAM_LOWER_ALIAS);

    when_str_empty_trace(higher_layer, exit, ERROR, "Failed to find the higher layer param");
    when_str_empty_trace(lower_layer, exit, ERROR, "Failed to find the lower layer param");
    when_str_empty_trace(higher_alias, exit, ERROR, "Failed to find the higher alias param");
    when_str_empty_trace(lower_alias, exit, ERROR, "Failed to find the lower alias param");

    inst = amxd_dm_findf(device_get_dm(), "%s[HigherLayer == '%s' && LowerLayer == '%s' && HigherAlias == '%s' && LowerAlias == '%s'].", INTERFACE_STACK_OBJECT, higher_layer, lower_layer, higher_alias, lower_alias);
    when_not_null(inst, exit); // an instance with the same parameters already exists, ignore it

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, INTERFACE_STACK_OBJECT);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_add_inst(&trans, 0, NULL);

    amxd_trans_set_value(cstring_t, &trans, "HigherLayer", higher_layer);
    amxd_trans_set_value(cstring_t, &trans, "LowerLayer", lower_layer);
    amxd_trans_set_value(cstring_t, &trans, "HigherAlias", higher_alias);
    amxd_trans_set_value(cstring_t, &trans, "LowerAlias", lower_alias);

    status = amxd_trans_apply(&trans, device_get_dm());
    amxd_trans_clean(&trans);
    when_failed_trace(status, exit, ERROR, "Failed to create an interface stack instance");
exit:
    return;
}

static void device_handle_interface_stack_event_unlink_cb(UNUSED const char* const sig_name,
                                                          const amxc_var_t* const data,
                                                          UNUSED void* const priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_trans_t trans;
    amxd_object_t* inst = NULL;
    const cstring_t higher_layer = NULL;
    const cstring_t lower_layer = NULL;
    const cstring_t higher_alias = NULL;
    const cstring_t lower_alias = NULL;

    when_null(data, exit);

    higher_layer = GET_CHAR(data, DEVICE_INTERFACE_STACK_EVENT_PARAM_HIGHER_LAYER);
    lower_layer = GET_CHAR(data, DEVICE_INTERFACE_STACK_EVENT_PARAM_LOWER_LAYER);
    higher_alias = GET_CHAR(data, DEVICE_INTERFACE_STACK_EVENT_PARAM_HIGHER_ALIAS);
    lower_alias = GET_CHAR(data, DEVICE_INTERFACE_STACK_EVENT_PARAM_LOWER_ALIAS);

    when_str_empty_trace(higher_layer, exit, ERROR, "Failed to find the higher layer param");
    when_str_empty_trace(lower_layer, exit, ERROR, "Failed to find the lower layer param");
    when_str_empty_trace(higher_alias, exit, ERROR, "Failed to find the higher alias param");
    when_str_empty_trace(lower_alias, exit, ERROR, "Failed to find the lower alias param");

    inst = amxd_dm_findf(device_get_dm(), "%s[HigherLayer == '%s' && LowerLayer == '%s' && HigherAlias == '%s' && LowerAlias == '%s'].", INTERFACE_STACK_OBJECT, higher_layer, lower_layer, higher_alias, lower_alias);
    when_null_trace(inst, exit, ERROR, "Received an unlink event, but the instance that needs to be linked cannot be found");

    amxd_trans_init(&trans);
    amxd_trans_select_pathf(&trans, INTERFACE_STACK_OBJECT);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_del_inst(&trans, inst->index, NULL);

    status = amxd_trans_apply(&trans, device_get_dm());
    amxd_trans_clean(&trans);
    when_failed_trace(status, exit, ERROR, "Failed to remove an interface stack instance");
exit:
    return;
}

