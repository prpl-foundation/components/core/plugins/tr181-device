/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "device.h"
#include "device_diagnostics.h"

static int get_object_data(const char* object_path, amxc_var_t* data) {
    amxb_bus_ctx_t* ctx = NULL;
    int ret = -1;

    when_str_empty_trace(object_path, exit, ERROR, "Object name is not provided");
    when_null_trace(data, exit, ERROR, "Data has not initialized");

    ctx = amxb_be_who_has(object_path);
    when_null_trace(ctx, exit, ERROR, "Could not find the context of the %s", object_path);

    ret = amxb_get(ctx, object_path, 0, data, 1);
    when_failed_trace(ret, exit, ERROR, "Failed to get data from %s", object_path);

exit:
    return ret;

}

static amxc_string_t* get_serial_timestamp(void) {
    amxc_string_t* ret_val = NULL;
    amxc_var_t obj_data;
    struct tm* timeinfo = NULL;
    const char* serial_number = NULL;
    time_t current_time;
    char* timestamp = NULL;

    amxc_string_new(&ret_val, 0);
    amxc_var_init(&obj_data);

    timestamp = (char*) calloc(16, sizeof(char));
    when_null_trace(timestamp, exit, ERROR, "Could not allocate memory");

    time(&current_time);
    timeinfo = gmtime(&current_time);
    strftime(timestamp, 16, "%Y%m\%d_%H%M\%S", timeinfo);

    get_object_data("DeviceInfo.SerialNumber", &obj_data);
    serial_number = GETP_CHAR(&obj_data, "0.0.SerialNumber");
    when_str_empty_trace(serial_number, exit, ERROR, "Could not retrieve the SerialNumber");

    amxc_string_setf(ret_val, "/tmp/%s_%s.tgz", serial_number, timestamp);

exit:
    amxc_var_clean(&obj_data);
    free(timestamp);
    return ret_val;
}

static debug_info_status set_vendor_log_file(const char* file_name) {
    int ret = -1;
    debug_info_status status = error_internal;
    amxc_var_t obj_data;
    amxc_var_t args;
    amxc_var_t func_ret;
    amxc_var_t* args_data = NULL;
    amxb_bus_ctx_t* ctx = NULL;
    const char* vendor_alias = NULL;

    amxc_var_init(&obj_data);
    amxc_var_init(&func_ret);
    amxc_var_init(&args);

    get_object_data("DeviceInfo.GetDebugVendorFileAlias", &obj_data);
    vendor_alias = GETP_CHAR(&obj_data, "0.0.GetDebugVendorFileAlias");
    when_str_empty_trace(vendor_alias, exit, ERROR, "Could not retrieve the GetDebugVendorFileAlias");

    ctx = amxb_be_who_has("DeviceInfo.");
    when_null_trace(ctx, exit, ERROR, "Could not find the context of the DeviceInfo");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Alias", vendor_alias);
    args_data = amxc_var_add_key(amxc_htable_t, &args, "Data", NULL);
    amxc_var_add_key(int32_t, args_data, "MaximumSize", 0);
    amxc_var_add_key(cstring_t, args_data, "Name", file_name);
    amxc_var_add_key(bool, args_data, "Persistent", false);

    ret = amxb_call(ctx, "DeviceInfo", "SetVendorLogFile", &args, &func_ret, 5);
    when_failed_trace(ret, exit, ERROR, "Could not create new VendorLogFile instance [%s]", vendor_alias);
    status = complete;

exit:
    amxc_var_clean(&obj_data);
    amxc_var_clean(&args);
    amxc_var_clean(&func_ret);
    return status;
}

static const char* config_const_char(const char* key) {
    const char* value = GET_CHAR(amxo_parser_get_config(device_get_parser(), key), NULL);
    return value != NULL ? value : "";
}

/**
 * @brief
 * start function for getDebugInformation Tool
 *
 * @param check_function Callback function to be called when getDebugInformation finishes
 * @param sch Scheduler (Kill previous test before launching another one)
 * @param ret output arguments
 * @return amxd_status_t
 */
amxd_status_t debug_info_start(void (* check_function)(const char* const,
                                                       const amxc_var_t* const,
                                                       void* const),
                               debug_info_scheduler_t* sch,
                               amxc_var_t* ret) {
    amxd_status_t ret_status = amxd_status_unknown_error;
    int ret_val = -1;
    amxc_var_t mod_args;
    amxc_var_t mod_ret;
    amxp_subproc_t* proc = NULL;
    const char* debug_name = NULL;
    amxc_string_t* serial_timestamp = NULL;
    const char* output_file = NULL;

    amxc_var_init(&mod_args);
    amxc_var_init(&mod_ret);
    amxc_var_set_type(&mod_args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(&mod_ret, AMXC_VAR_ID_HTABLE);

    when_null_trace(ret, exit, ERROR, "Invalid argument");
    amxc_var_add_key(cstring_t, ret, DEBUG_INFO_RESULTS_KEY, NULL);
    amxc_var_add_key(cstring_t, ret, DEBUG_INFO_STATUS_KEY, DEBUG_INFO_STATUS_ERROR_OTHER);

    when_null_trace(check_function, exit, ERROR, "There is no check function");
    when_null_trace(sch, exit, ERROR, "Scheduler has not been initialized");

    if(sch->pid > 0) {
        debug_info_stop(sch);
    }

    debug_name = config_const_char("debugname");
    amxc_string_new(&(sch->file_name), 0);

    if((debug_name != NULL) && (strcmp(debug_name, "SERIAL_TIMESTAMP") == 0)) {
        serial_timestamp = get_serial_timestamp();
        output_file = amxc_string_get(serial_timestamp, 0);
        when_str_empty_trace(output_file, exit, ERROR, "Could not generate <serial>_<timestamp>.tgz file name");
    } else {
        output_file = DEBUG_FILEPATH;
    }
    amxc_string_set(sch->file_name, output_file);

    amxc_var_add_key(cstring_t, &mod_args, DEBUG_INFO_KEY_ALL, output_file);

    ret_val = amxm_execute_function(MOD_DEBUG_INFO_NAME, MOD_DEBUG_INFO_CTRL, "start-debug", &mod_args, &mod_ret);
    when_failed_trace(ret_val, exit, ERROR, "Failed to execute the start-debug function.");

    sch->pid = GET_INT32(&mod_ret, DEBUG_RET_KEY_PROC_PID);
    when_true_trace(sch->pid <= 0, exit, ERROR, "No process id after executing the start-debug function");

    proc = amxp_subproc_find(sch->pid);
    when_null_trace(proc, exit, ERROR, "No sub process found after executing the start-debug function");

    ret_val = amxp_slot_connect(proc->sigmngr, "stop", NULL, check_function, sch);
    when_true_trace(ret_val != 0, exit, ERROR, "Failed to register slot for stop signal");

    ret_status = amxd_status_deferred;
exit:
    amxc_string_delete(&serial_timestamp);
    amxc_var_clean(&mod_args);
    amxc_var_clean(&mod_ret);
    return ret_status;
}

/**
 * @brief
 * Function that verify if the current test has finished successfully or not (callback function).
 * Provides the final status of the deferred function.
 *
 * @param event_name
 * @param data Event data, contains exit code from the getDebugInformation process
 * @param priv Private data, contains the scheduler
 */
void debug_info_sched_check(UNUSED const char* const event_name,
                            const amxc_var_t* const data,
                            void* const priv) {
    amxc_var_t* mod_ret = NULL;
    amxc_var_t* mod_args = NULL;
    debug_info_status status = not_complete;
    debug_info_scheduler_t* sch = (debug_info_scheduler_t*) priv;
    const char* file_name = NULL;

    amxc_var_new(&mod_ret);
    amxc_var_new(&mod_args);
    amxc_var_set_type(mod_ret, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(mod_args, AMXC_VAR_ID_HTABLE);

    when_null(data, exit);
    when_null(GET_ARG(data, DEBUG_RET_KEY_EXIT_CODE), exit);
    when_null(sch, exit);

    file_name = amxc_string_get(sch->file_name, 0);

    amxc_var_add_key(uint32_t, mod_args, DEBUG_RET_KEY_EXIT_CODE, GET_UINT32(data, DEBUG_RET_KEY_EXIT_CODE));
    amxc_var_add_key(cstring_t, mod_args, DEBUG_INFO_KEY_FILEPATH, file_name);

    status = amxm_execute_function(MOD_DEBUG_INFO_NAME, MOD_DEBUG_INFO_CTRL, "check-debug", mod_args, mod_ret);

    if(status == complete) {
        status = set_vendor_log_file(file_name);
    }

    if(status == complete) {
        amxd_function_deferred_done(sch->call_id, amxd_status_ok, mod_ret, NULL);
    } else if((status == error_other) || (status == error_internal)) {
        amxd_function_deferred_done(sch->call_id, amxd_status_unknown_error, mod_ret, NULL);
    }

    // Subprocess terminated with child
    sch->pid = 0;
    amxc_string_delete(&(sch->file_name));

exit:
    amxc_var_delete(&mod_args);
    amxc_var_delete(&mod_ret);
}

/**
 * @brief
 * Stop function for getDebugInformation tool
 *
 * @param sched Scheduler (Kill previous test before launching another one)
 * @return amxd_status_t
 */
amxd_status_t debug_info_stop(debug_info_scheduler_t* sch) {
    amxc_var_t* mod_ret = NULL;
    amxc_var_t* mod_args = NULL;
    int32_t rv = -1;

    amxc_var_new(&mod_args);
    amxc_var_new(&mod_ret);
    amxc_var_set_type(mod_args, AMXC_VAR_ID_HTABLE);
    amxc_var_set_type(mod_ret, AMXC_VAR_ID_HTABLE);

    when_null_trace(sch, exit, ERROR, "Invalid argument");

    amxc_var_add_key(int32_t, mod_args, DEBUG_RET_KEY_PROC_PID, sch->pid);

    rv = amxm_execute_function(MOD_DEBUG_INFO_NAME, MOD_DEBUG_INFO_CTRL, "stop-debug", mod_args, mod_ret);

    sch->pid = 0;
    amxc_string_delete(&(sch->file_name));

exit:
    amxd_function_deferred_done(sch->call_id, amxd_status_ok, mod_ret, NULL);
    amxc_var_delete(&mod_args);
    amxc_var_delete(&mod_ret);
    return rv;
}
