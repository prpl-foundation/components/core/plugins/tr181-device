/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**********************************************************
* Include files
**********************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//used for reboot
#include <sys/reboot.h>    /* Definition of RB_* constants */
#include <unistd.h>

#include "device.h"
#include "device_rpc_calls.h"
#include "device_diagnostics.h"

#include <amxd/amxd_object_event.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

/**********************************************************
* Macro definitions
**********************************************************/
#define RBCOMMAND "/sbin/reboot"
#define STR_EMPTY(_str) ((_str == NULL) || (_str[0] == '\0'))

#define IS_REBOOT_CAUSE_VALID(cause) is_valid_args(reboot_causes, sizeof(reboot_causes) / sizeof(reboot_causes[0]), cause)
#define IS_FACTORY_RESET_CAUSE_VALID(cause) is_valid_args(facreset_causes, sizeof(facreset_causes) / sizeof(facreset_causes[0]), cause)

/**********************************************************
* Type definitions
**********************************************************/

typedef enum {
    REBOOT,
    FACTORY_RESET
} reboot_type_t;

/**********************************************************
* Variable declarations
**********************************************************/
amxp_timer_t* reboot_timer = NULL;

static debug_info_scheduler_t scheduler = {
    .pid = 0,
    .call_id = 0
};

const cstring_t reboot_causes[] = {
    "LocalReboot",      // Default value. Keep @[0]
    "RemoteReboot",
};

const cstring_t facreset_causes[] = {
    "LocalFactoryReset",   // Default value. Keep @[0]
    "RemoteFactoryReset",
};

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

/* Validate a cstring_t using an array of acceptable arguments */
static bool is_valid_args(const cstring_t valid_tab[], int sz, const cstring_t arg) {
    bool is_valid = false;

    when_null(arg, exit);
    for(int i = 0; i < sz; i++) {
        when_true_status(strcmp(arg, valid_tab[i]) == 0, exit, is_valid = true);
    }
exit:
    return is_valid;
}

static amxd_status_t add_reboot_entry(reboot_type_t type, const cstring_t cause, const cstring_t reason) {
    int status = -1;
    amxc_var_t entry_args;
    amxb_bus_ctx_t* reboot_ctx = amxb_be_who_has("Reboot.Reboot.");
    amxd_status_t ret_status = amxd_status_invalid_arg;

    amxc_var_init(&entry_args);
    amxc_var_set_type(&entry_args, AMXC_VAR_ID_HTABLE);

    switch(type) {
    case FACTORY_RESET:
        if(STR_EMPTY(cause)) {
            cause = "LocalFactoryReset";
        } else {
            when_false_trace(IS_FACTORY_RESET_CAUSE_VALID(cause), exit, ERROR, "Invalid argument (%s)", cause)
        }
        break;
    case REBOOT:
    default:
        if(STR_EMPTY(cause)) {
            cause = "LocalReboot";
        } else {
            when_false_trace(IS_REBOOT_CAUSE_VALID(cause), exit, ERROR, "Invalid argument (%s)", cause)
        }
        break;
    }

    if(STR_EMPTY(reason)) {
        reason = "Initiated by Unknown";
    }

    amxc_var_add_key(cstring_t, &entry_args, "Reason", reason);
    amxc_var_add_key(cstring_t, &entry_args, "Cause", cause);

    status = amxb_call(reboot_ctx, "Reboot.", "reboot", &entry_args, NULL, 5);
    if(status != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to insert a new entry into Reboot.Reboot[] (%d)", status);
        ret_status = amxd_status_unknown_error;
        goto exit;
    }

    ret_status = amxd_status_ok;

exit:
    amxc_var_clean(&entry_args);
    return ret_status;
}

static void reboot_timer_cb(UNUSED amxp_timer_t* timer, UNUSED void* priv) {
    SAH_TRACEZ_WARNING(ME, "Reboot timer expired, force an immediate shutdown");
    int status = -1;

    amxp_timer_stop(reboot_timer);
    amxp_timer_delete(&reboot_timer);

    status = reboot(RB_AUTOBOOT);
    if(status == -1) {
        SAH_TRACEZ_ERROR(ME, "Failed to reboot, with error code: (%i)", status);
    } else {
        SAH_TRACEZ_INFO(ME, "Successful reboot");
    }
}

static int reboot_device(void) {
    int timeout = GET_UINT32(&device_get_parser()->config, "reboot_timeout");
    int status = -1;

    /* Shouldn't we check if a reboot_timer already exists? */
    amxp_timer_new(&reboot_timer, reboot_timer_cb, NULL);
    amxp_timer_start(reboot_timer, timeout * 1000);

    status = system(RBCOMMAND);
    if(status != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to reboot, with error code: (%i)", status);
    } else {
        SAH_TRACEZ_INFO(ME, "Successful reboot");
    }

    return status;
}

amxd_status_t _Reboot(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func,
                      amxc_var_t* args, UNUSED amxc_var_t* ret) {

    amxd_status_t ret_status = amxd_status_unknown_error;
    const cstring_t cause = GET_CHAR(args, "Cause");
    const cstring_t reason = GET_CHAR(args, "Reason");

    // Only handle this error, as the reboot service isn't always available.
    when_true(add_reboot_entry(REBOOT, cause, reason) == amxd_status_invalid_arg, exit);
    when_failed(reboot_device(), exit);
    ret_status = amxd_status_ok;
exit:
    return ret_status;
}

amxd_status_t _SendBootEvent(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func, UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret) {
    amxd_status_t ret_status = amxd_status_unknown_error;

    amxd_object_emit_signal(object, "Boot!", NULL);
    ret_status = amxd_status_ok;

    return ret_status;
}

static int reset_hard(void) {
    int status = -1;
    amxp_subproc_t* proc = NULL;

    amxp_subproc_new(&proc);
#if CONFIG_SAH_AMX_TR181_DEVICE_RESET_HARD_ENABLE
    amxp_subproc_start_wait(proc, 2000, (char*) "reset_hard", NULL);
    SAH_TRACEZ_INFO(ME, "Factory_Reset execute reset_hard");
#else
    amxp_subproc_start_wait(proc, 2000, (char*) "firstboot", "-y", NULL);
    SAH_TRACEZ_INFO(ME, "Factory_Reset execute firstboot");
#endif

    status = amxp_subproc_get_exitstatus(proc);
    when_failed_trace(status, exit, ERROR, "Failed to execute factory reset command, with error code: (%i)", status);
    status = 0;

exit:
    amxp_subproc_delete(&proc);
    return status;
}

amxd_status_t _FactoryReset(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func,
                            amxc_var_t* args, UNUSED amxc_var_t* ret) {
    amxd_status_t ret_status = amxd_status_unknown_error;
    const cstring_t cause = GET_CHAR(args, "Cause");
    const cstring_t reason = GET_CHAR(args, "Reason");

    // Only handle this error, as the reboot service isn't always available.
    when_true(add_reboot_entry(FACTORY_RESET, cause, reason) == amxd_status_invalid_arg, exit);
    when_failed(reset_hard(), exit);
    SAH_TRACEZ_INFO(ME, "Successfully executed factory reset");
    when_failed_trace(reboot_device(), exit, ERROR, "Failed to reboot after successful factory reset. Try rebooting manually");
    ret_status = amxd_status_ok;
exit:
    return ret_status;
}

amxd_status_t _SelfTestDiagnostics(UNUSED amxd_object_t* object, amxd_function_t* func, amxc_var_t* args, amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    status = debug_info_start(debug_info_sched_check, &scheduler, args);
    when_true_trace(status != amxd_status_deferred, exit, ERROR, "Failed to call the getdDebugInformation tool");

    amxd_function_defer(func, &(scheduler.call_id), ret, NULL, NULL);
exit:
    return status;
}

amxd_status_t _PacketCaptureDiagnostics(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func, UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret) {
    return amxd_status_ok;
}

amxd_status_t _ScheduleTimer(UNUSED amxd_object_t* object, UNUSED amxd_function_t* func, UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret) {

    return amxd_status_ok;
}
