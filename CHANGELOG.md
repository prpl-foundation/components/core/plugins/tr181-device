# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.30.0 - 2024-12-10(11:54:52 +0000)

### Changes

- Reboot: Update module for compatibility with reboot service that implement TR-181 2.18

### Other

- [prpl][reboot-service] Implement TR-181 2.18 refactor Device.DeviceInfo.Reboots.{i}.Reason / add Device.DeviceInfo.Reboots.{i}.FirmwareUpdated (break compatibility)

## Release v0.29.8 - 2024-12-04(08:12:43 +0000)

### Other

- - [device] remove the debug information for the whole DM

## Release v0.29.7 - 2024-09-26(11:45:31 +0000)

### Other

- - ScheduleTimer not registered with USP
- - ScheduleTimer not registered with USP

## Release v0.29.6 - 2024-09-24(15:45:36 +0000)

### Other

- - ScheduleTimer not registered with USP
- - ScheduleTimer not registered with USP

## Release v0.29.5 - 2024-09-10(07:07:12 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v0.29.4 - 2024-07-23(07:51:01 +0000)

### Changes

- [TR181-Device]Bidirectional communication support between UBUS and IMTP

## Release v0.29.3 - 2024-07-20(06:10:28 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v0.29.2 - 2024-07-18(15:35:52 +0000)

### Other

- - [Prpl][CI][tr181-device] Plugins exits on start in reboot scenario (amxs synchronizaiton failed)

## Release v0.29.1 - 2024-06-20(08:36:37 +0000)

### Other

- - It is not possible to invoke gi on Device.DHCPv4.Client.
- - Add Device.DHCPv4.ClientNumberOfEntries and Device.DHCPv6.ClientNumberOfEntries

## Release v0.29.0 - 2024-06-12(15:10:11 +0000)

### New

- Wrong default cause for Device.Reboot()

## Release v0.28.1 - 2024-04-19(11:33:41 +0000)

### Other

- [ProcessMonitor] Add tr181-device plugin to process monitor

## Release v0.28.0 - 2024-04-18(15:04:40 +0000)

### New

- Use mod-usp-registration for registering data models to obuspa

## Release v0.27.3 - 2024-04-10(10:04:49 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.27.2 - 2024-03-26(16:27:01 +0000)

### Other

- [USP] Add register retry mechanism

## Release v0.27.1 - 2024-03-25(16:24:20 +0000)

### Other

- fix typo in odl file

## Release v0.27.0 - 2024-03-25(12:17:04 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.26.1 - 2024-03-21(09:06:36 +0000)

### Fixes

- After reboot all hosts are disconnected (AKA amb timeouts)

## Release v0.26.0 - 2024-03-20(21:13:45 +0000)

### New

- [LogFile Format] The logfile must have a dedicated format

## Release v0.25.0 - 2024-03-15(16:00:11 +0000)

### New

- [TR181-DeviceInfo]File upload enhancements

## Release v0.24.2 - 2024-03-07(14:47:46 +0000)

### Fixes

- [Safran_PRPL][Reboot plugin] Unable to detect the boot reason automatically and the BootCounter is not incrementing

## Release v0.24.1 - 2024-03-05(11:11:41 +0000)

### Other

- [Device] Missing notifications when a plugin restarts

## Release v0.24.0 - 2024-03-04(09:56:19 +0000)

### New

- [LED] Support for Device.­Device­Info.­Temperature­Status.­Temperature­Sensor.­{i}. Data Model parameters.

## Release v0.23.1 - 2024-03-01(15:55:12 +0000)

### Other

- [TR181-Device] Update Device.DataModelVersion to 2.17

## Release v0.23.0 - 2024-02-21(08:37:43 +0000)

### New

- [TR181-Syslog] Implementation of the new 2.17 BBF Data model definition Syslog.

## Release v0.22.0 - 2024-02-15(16:31:22 +0000)

### New

- Move proxy mappings to the individual components

## Release v0.21.2 - 2024-01-11(09:50:22 +0000)

### Other

- map WiFiScheduler to Device.WiFiScheduler

## Release v0.21.1 - 2024-01-09(15:48:01 +0000)

### Other

- Disable OBUSPA support on internal configs

## Release v0.21.0 - 2024-01-08(17:14:32 +0000)

### New

- Allow creating subscriptions via LocalAgent.Subscription dm

## Release v0.20.4 - 2024-01-04(12:40:20 +0000)

### Other

- Add Device.GatewayInfo to the repeater config

## Release v0.20.3 - 2023-12-15(07:44:21 +0000)

### Other

- mod-wifimapper stops tr181-device

## Release v0.20.2 - 2023-12-08(13:41:50 +0000)

## Release v0.20.1 - 2023-12-05(12:08:42 +0000)

### Fixes

- [tr181-device] Device.FactoryReset() doing a firstboot (iso reset_hard)

## Release v0.20.0 - 2023-12-05(11:13:59 +0000)

### Other

- : Add BBF parameters

## Release v0.19.0 - 2023-12-03(10:03:15 +0000)

### New

- [TR181-Schedules] Implementation of the Schedules Data model. (BBF DEV2DM-562)

## Release v0.18.0 - 2023-12-01(09:06:34 +0000)

## Release v0.17.5 - 2023-11-30(16:10:03 +0000)

### Fixes

- [tr181-device] Parsing error on the access point ODL.

## Release v0.17.4 - 2023-11-29(14:38:41 +0000)

### Other

- Add BBF parameters

## Release v0.18.0 - 2023-11-29(14:27:40 +0000)

### Other

- Add BBF parameters

## Release v0.17.3 - 2023-11-29(14:22:03 +0000)

### Other

- [CHR2] The Device.XPON. object should be removed form the Device mapper

## Release v0.17.2 - 2023-11-29(10:01:21 +0000)

### Fixes

- WAN port instability

## Release v0.17.1 - 2023-11-28(09:22:32 +0000)

### Changes

- [USP] Plugins that use the USP backend need to load it explicitly

## Release v0.17.0 - 2023-11-20(12:19:03 +0000)

### New

- [USP] Set up communication with obuspa

## Release v0.16.0 - 2023-11-13(09:10:47 +0000)

### Other

- CaptivePortal should be mapped in device plugin

## Release v0.15.4 - 2023-11-09(08:28:50 +0000)

### Other

- The verify function is missing

## Release v0.15.3 - 2023-10-13(14:12:17 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v0.15.2 - 2023-09-28(07:56:54 +0000)

### Fixes

- [AP Config] List of the mapped devices must be different on AP and HGW

## Release v0.15.1 - 2023-09-22(09:50:19 +0000)

### Other

- - [prpl][tr181-device] LEDs should be accessible under Device.

## Release v0.15.0 - 2023-08-31(09:56:52 +0000)

### New

- Move datamodel prefixes to the device proxy

## Release v0.14.3 - 2023-08-29(08:55:29 +0000)

### Fixes

- [tr181-dhcpv4][tr181-device] Subscription misses events if subscribed on Device proxy

## Release v0.14.2 - 2023-07-31(12:07:47 +0000)

### Fixes

- [tr181-dhcpv4relay] DHCPv4Relay not mapped in device plugin

## Release v0.14.1 - 2023-07-25(13:19:39 +0000)

### Fixes

- [tr181-device] SelfTestDiagnostic can be done with 1 less child process

## Release v0.14.0 - 2023-07-24(14:07:07 +0000)

### New

-  [amx][conmon] ConMon must be available as a prpl plugin

## Release v0.13.1 - 2023-07-17(07:25:06 +0000)

### Other

- [TR181-Homeplug] Implementation of a TR181 compliant Homeplug plugin

## Release v0.13.0 - 2023-07-12(09:54:26 +0000)

### New

- [amx][Device] implementation of the SelfTest function

## Release v0.12.0 - 2023-06-22(07:00:39 +0000)

### New

- add ACLs permissions for cwmp user

## Release v0.11.0 - 2023-06-05(11:34:43 +0000)

### Other

- [Button Manager][prpl][amx] The different buttons and the button State should be exposed in the DM (HL API)

## Release v0.10.0 - 2023-05-15(14:26:50 +0000)

### New

- Add xpon

## Release v0.9.0 - 2023-05-15(14:07:41 +0000)

### New

- [amx][prpl]Implementation of the LANConfigSecurity module

## Release v0.8.2 - 2023-03-23(08:28:42 +0000)

### Other

- - [ADD] Mapping for UPnPDiscovery and UPnPDescription

## Release v0.8.1 - 2023-03-15(11:07:28 +0000)

### Other

- TR-069 Management server is wrongly mapped

## Release v0.8.0 - 2023-03-13(16:12:39 +0000)

### Fixes

- [DHCPv4] Split client and server plugin

## Release v0.7.3 - 2023-03-13(08:09:01 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v0.7.2 - 2023-01-17(08:27:40 +0000)

### Other

- Implement autosensing directly in the DM/plugin

## Release v0.7.1 - 2022-12-09(09:18:08 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v0.7.0 - 2022-12-02(12:48:00 +0000)

### Other

- [Packet Interception] Create the new Packet Interception component

## Release v0.6.1 - 2022-11-18(14:23:15 +0000)

### Other

- add Security to Device.Security datamodel

## Release v0.6.0 - 2022-10-24(15:48:57 +0000)

### New

- implement interface stack

## Release v0.5.2 - 2022-10-14(09:08:34 +0000)

### Other

- [HTTPAccess] Integration of HTTP Access plugin

## Release v0.5.1 - 2022-09-23(11:19:15 +0000)

### Other

- [Device] PCP and the DSLite data model must be mapped to Device.PCP/DSlite.

## Release v0.5.0 - 2022-09-15(07:37:46 +0000)

### Other

- [Automatic Testing] Add simple checks for the Device.X data model

## Release v0.4.9 - 2022-09-12(06:24:56 +0000)

### Other

- [Device][SSH] SSH is no longer correctly mapped to Device.SSH

## Release v0.4.8 - 2022-09-09(14:01:48 +0000)

### Other

- Improve unit testing of the tr181-device plugin.

## Release v0.4.7 - 2022-09-08(06:41:55 +0000)

### Fixes

- [PRPL] The Device. object is empty

## Release v0.4.6 - 2022-09-01(10:23:03 +0000)

### Other

- [Device][DMProxy] Integration of new dmproxy module in prpl.

## Release v0.4.5 - 2022-07-05(07:04:43 +0000)

### Fixes

- Fix device reboot to gracefully stop the system

## Release v0.4.4 - 2022-05-19(12:37:53 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.4.3 - 2022-04-07(11:08:53 +0000)

### Fixes

- Boot event must contain exclamation mark

## Release v0.4.2 - 2022-03-24(10:48:28 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.4.1 - 2022-02-25(11:57:04 +0000)

### Other

- Enable core dumps by default

## Release v0.4.0 - 2021-12-08(16:10:58 +0000)

### New

- Add SendBootEvent function to the tr181-device component

## Release v0.3.2 - 2021-12-01(11:54:33 +0000)

### Changes

- Use module mod-dmproxy

## Release v0.3.1 - 2021-11-23(17:14:44 +0000)

### Other

- [ACL] The Device plugin must have a default ACL configuration

## Release v0.3.0 - 2021-10-20(08:31:06 +0000)

### New

- [tr181-device] implement interface stack

## Release v0.2.2 - 2021-09-16(13:17:05 +0000)

### Fixes

- [TR181-Device] add sahtrace param

## Release v0.2.1 - 2021-09-16(09:22:43 +0000)

### Fixes

- [TR181-Device] add sahtrace param

## Release v0.2.0 - 2021-09-15(06:58:23 +0000)

### New

- [tr181-Device] add extra documentation to odl

## Release v0.1.0 - 2021-09-10(12:53:13 +0000)

### New

- [GMAP-CLIENT] implement proxy, reboot, factoryreset functions

## Release v0.0.2 - 2021-09-07(15:50:32 +0000)

### Fixes

- [tr181-device]develop the base of the tr181-device plugin

