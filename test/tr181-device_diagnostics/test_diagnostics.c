/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <signal.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_types.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include "dummy_be.h"
#include "test_diagnostics.h"
#include "device.h"
#include "device_rpc_calls.h"
#include "device_diagnostics.h"

static const char* odl_import = "mod_dmproxy.odl";

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;
static debug_info_status check_debug_ret_status;
static amxd_status_t start_debug_ret_status;

static debug_info_scheduler_t scheduler_local = {.pid = 0,
    .call_id = 0};

extern amxd_status_t __real_debug_info_start(void (* check_function)(const char* const,
                                                                     const amxc_var_t* const,
                                                                     void* const),
                                             debug_info_scheduler_t* sch,
                                             amxc_var_t* ret);

int __wrap_amxm_execute_function(const char* const shared_object_name,
                                 const char* const module_name,
                                 const char* const func_name,
                                 amxc_var_t* args,
                                 amxc_var_t* ret);

int __wrap_amxm_execute_function(UNUSED const char* const shared_object_name,
                                 UNUSED const char* const module_name,
                                 const char* const func_name,
                                 UNUSED amxc_var_t* args,
                                 amxc_var_t* ret) {

    if((NULL != func_name) && (0 == strcmp(func_name, "start-debug"))) {
        amxc_var_add_key(int32_t, ret, DEBUG_RET_KEY_PROC_PID, 99);
        return start_debug_ret_status;
    } else if((NULL != func_name) && (0 == strcmp(func_name, "check-debug"))) {
        return check_debug_ret_status;
    }
    return 0;
}

amxp_subproc_t* __wrap_amxp_subproc_find(UNUSED const int pid);

amxp_subproc_t* __wrap_amxp_subproc_find(UNUSED const int pid) {
    amxp_subproc_t* test = NULL;
    amxp_subproc_new(&test);
    return test;
}

amxd_status_t __wrap_debug_info_start(void (* check_function)(const char* const,
                                                              const amxc_var_t* const,
                                                              void* const),
                                      debug_info_scheduler_t* sch,
                                      amxc_var_t* ret);

amxd_status_t __wrap_debug_info_start(void (* check_function)(const char* const,
                                                              const amxc_var_t* const,
                                                              void* const),
                                      UNUSED debug_info_scheduler_t* sch,
                                      amxc_var_t* ret) {
    amxd_status_t status = amxd_status_unknown_error;
    status = __real_debug_info_start(check_function, &scheduler_local, ret);
    return status;
}
int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx, const char* object, const char* method,
                     amxc_var_t* args, amxc_var_t* ret, int timeout);

int __wrap_amxb_call(UNUSED amxb_bus_ctx_t* const bus_ctx, UNUSED const char* object, const char* method,
                     UNUSED amxc_var_t* args, UNUSED amxc_var_t* ret, UNUSED int timeout) {
    int ret_val = 1;
    when_str_empty(method, exit);
    if(strcmp(method, "SetVendorLogFile") == 0) {
        ret_val = 0;
    }

exit:
    return ret_val;

}

int __wrap_amxp_slot_connect(amxp_signal_mngr_t* const sig_mngr,
                             const char* const sig_name,
                             const char* const expression,
                             amxp_slot_fn_t fn,
                             void* const priv);

int __wrap_amxp_slot_connect(UNUSED amxp_signal_mngr_t* const sig_mngr,
                             UNUSED const char* const sig_name,
                             UNUSED const char* const expression,
                             UNUSED amxp_slot_fn_t fn,
                             UNUSED void* const priv) {

    return 0;
}

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

// read sig alarm is used to wait for a timer to run out so the callback
// function for this timer will be executed before continuing with the
// next test. In these tests it is used to wait on the delayed hard reboot
UNUSED static void read_sig_alarm(void) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    assert_int_equal(s, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        amxp_timers_calculate();
        amxp_timers_check();
        handle_events();
    }
}

static void connection_read(UNUSED int fd, UNUSED void* priv) {
    // Do nothing
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);

    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    amxb_register(bus_ctx, &dm);
    amxo_resolver_ftab_add(&parser, "SelfTestDiagnostics", AMXO_FUNC(_SelfTestDiagnostics));

    assert_int_equal(amxo_parser_parse_file(&parser, odl_import, root_obj), 0);
    assert_int_equal(_device_main(0, &dm, &parser), 0);

    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(_device_main(1, &dm, &parser), 0);

    amxb_free(&bus_ctx);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    test_unregister_dummy_be();
    return 0;
}

void test_get_debug_info(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(&dm, "Device.");
    amxd_status_t ret_status = amxd_status_unknown_error;
    amxd_function_t* func = NULL;
    amxc_var_t* ret = NULL;
    amxc_var_t* args = NULL;
    amxc_var_t data;

    amxc_var_new(&ret);
    amxc_var_new(&args);
    amxc_var_init(&data);

    assert_non_null(object);
    func = amxd_object_get_function(object, "SelfTestDiagnostics");

    //start getdebuginformation tool succesfully
    start_debug_ret_status = amxd_status_ok;
    ret_status = _SelfTestDiagnostics(object, func, args, ret);
    assert_int_equal(ret_status, amxd_status_deferred);

    //cannot start the getdebuginformation tool
    start_debug_ret_status = amxd_status_unknown_error;
    ret_status = _SelfTestDiagnostics(object, func, args, ret);
    assert_int_equal(ret_status, amxd_status_unknown_error);

    //getdebuginformation tool finish successfully
    check_debug_ret_status = complete;
    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(int32_t, &data, DEBUG_RET_KEY_EXIT_CODE, complete);

    debug_info_sched_check(NULL, &data, &scheduler_local);
    amxc_var_clean(&data);

    //getdebugInformation tool return internal error
    check_debug_ret_status = error_internal;
    debug_info_sched_check(NULL, NULL, NULL);

    //getdebugInformation tool return other error
    check_debug_ret_status = error_other;
    debug_info_sched_check(NULL, NULL, NULL);

    amxc_var_delete(&ret);
    amxc_var_delete(&args);
}

void test_get_debug_info_multiple(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(&dm, "Device.");
    amxd_status_t status = amxd_status_unknown_error;
    amxc_var_t* args = NULL;
    amxd_function_t* func = NULL;
    amxc_var_t* ret = NULL;

    assert_non_null(object);
    func = amxd_object_get_function(object, "SelfTestDiagnostics");

    amxc_var_new(&args);
    amxc_var_new(&ret);
    amxc_var_set_type(args, AMXC_VAR_ID_HTABLE);

    scheduler_local.pid = 88;
    start_debug_ret_status = amxd_status_ok;
    status = _SelfTestDiagnostics(object, func, args, ret);
    assert_int_equal(status, amxd_status_deferred);

    // Restarted with same mock PID
    assert_int_equal(scheduler_local.pid, 99);

    amxc_var_delete(&args);
    amxc_var_delete(&ret);
}
