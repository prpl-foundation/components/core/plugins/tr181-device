/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <signal.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_types.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb_register.h>
#include <amxo/amxo.h>

#include "dummy_be.h"
#include "test_rpcs.h"
#include "device.h"
#include "device_rpc_calls.h"
#include "device_diagnostics.h"

static const char* odl_import = "mod_dmproxy.odl";

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t* bus_ctx = NULL;

int __real_system(const char* cmd);
int __wrap_system(const char* cmd);

int __wrap_system(const char* cmd) {
    if(strcmp(cmd, "/sbin/reboot") == 0) {
        __real_system("mock_reboot");
        return 0;
    } else {
        return __real_system(cmd);
    }
}

amxp_subproc_t* __wrap_amxp_subproc_find(UNUSED const int pid);

amxp_subproc_t* __wrap_amxp_subproc_find(UNUSED const int pid) {
    amxp_subproc_t* test = NULL;
    amxp_subproc_new(&test);
    return test;
}

int __wrap_amxp_slot_connect(amxp_signal_mngr_t* const sig_mngr,
                             const char* const sig_name,
                             const char* const expression,
                             amxp_slot_fn_t fn,
                             void* const priv);

int __wrap_amxp_slot_connect(UNUSED amxp_signal_mngr_t* const sig_mngr,
                             UNUSED const char* const sig_name,
                             UNUSED const char* const expression,
                             UNUSED amxp_slot_fn_t fn,
                             UNUSED void* const priv) {

    return 0;
}

int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     int timeout);

int __wrap_amxb_call(UNUSED amxb_bus_ctx_t* const bus_ctx,
                     UNUSED const char* object,
                     UNUSED const char* method,
                     amxc_var_t* args,
                     UNUSED amxc_var_t* ret,
                     UNUSED int timeout) {
    check_expected(args);

    return 0;
}

static int validate_args(const LargestIntegralType value,
                         const LargestIntegralType check_value_data) {
    amxc_var_t* var_value_to_check = (amxc_var_t*) value;
    amxc_var_t* var_check_value = (amxc_var_t*) check_value_data;
    int ret = 0;

    amxc_var_dump(var_value_to_check, 0);
    amxc_var_dump(var_check_value, 0);
    amxc_var_compare(var_value_to_check, var_check_value, &ret);

    return !ret;
}

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

// read sig alarm is used to wait for a timer to run out so the callback
// function for this timer will be executed before continuing with the
// next test. In these tests it is used to wait on the delayed hard reboot
static void read_sig_alarm(void) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    assert_int_equal(s, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        amxp_timers_calculate();
        amxp_timers_check();
        handle_events();
    }
}

static void connection_read(UNUSED int fd, UNUSED void* priv) {
    // Do nothing
}

static bool test_verify_data(const amxc_var_t* data, const char* field, const char* value) {
    bool rv = false;
    char* field_value = NULL;
    amxc_var_t* field_data = GETP_ARG(data, field);

    printf("Verify table data: check field [%s] contains [%s]\n", field, value);
    fflush(stdout);
    assert_non_null(field_data);

    field_value = amxc_var_dyncast(cstring_t, field_data);
    assert_non_null(field_data);

    rv = (strcmp(field_value, value) == 0);

    free(field_value);
    return rv;
}

static void check_boot_event(UNUSED const char* const sig_name,
                             const amxc_var_t* const data,
                             UNUSED void* const priv) {
    amxc_var_dump(data, STDOUT_FILENO);

    assert_true(test_verify_data(data, "notification", "Boot!"));
    assert_true(test_verify_data(data, "path", "Device."));
    assert_true(test_verify_data(data, "object", "Device."));
    assert_true(test_verify_data(data, "eobject", "Device."));
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock"), 0);

    amxo_connection_add(&parser, 101, connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx);
    amxb_register(bus_ctx, &dm);
    amxo_resolver_ftab_add(&parser, "SendBootEvent", AMXO_FUNC(_SendBootEvent));
    amxo_resolver_ftab_add(&parser, "SelfTestDiagnostics", AMXO_FUNC(_SelfTestDiagnostics));

    assert_int_equal(amxo_parser_parse_file(&parser, odl_import, root_obj), 0);
    assert_int_equal(_device_main(0, &dm, &parser), 0);

    assert_int_equal(system("cp firstboot /usr/local/bin"), 0);
    assert_int_equal(system("chmod +x /usr/local/bin/firstboot"), 0);

    return 0;
}

int test_teardown(UNUSED void** state) {
    assert_int_equal(_device_main(1, &dm, &parser), 0);

    amxb_free(&bus_ctx);
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    remove("/usr/local/bin/firstboot");
    remove("/usr/local/bin/mock_reboot");
    remove("fb");
    remove("rb");

    test_unregister_dummy_be();
    return 0;
}

void test_can_send_boot_event(UNUSED void** state) {
    int retval = 0;
    amxd_object_t* object = amxd_dm_findf(&dm, "Device.");

    retval = amxb_subscribe(bus_ctx, "Device.", "(notification == 'Boot!')", check_boot_event, NULL);
    assert_int_equal(retval, 0);
    assert_non_null(object);
    _SendBootEvent(object, NULL, NULL, NULL);

    handle_events();
}

void test_factory_reset_immediate_reboot(UNUSED void** state) {
    amxd_object_t* object = amxd_dm_findf(&dm, "Device.");
    amxd_status_t ret_status = amxd_status_unknown_error;
    amxc_var_t test_args;

    assert_non_null(object);

    assert_int_equal(system("cp mock_reboot /usr/local/bin"), 0);
    assert_int_equal(system("chmod +x /usr/local/bin/mock_reboot"), 0);

    amxc_var_init(&test_args);
    amxc_var_set_type(&test_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &test_args, "Cause", "LocalFactoryReset");
    amxc_var_add_key(cstring_t, &test_args, "Reason", "Initiated by Unknown");
    expect_check(__wrap_amxb_call, args, validate_args, &test_args);

    ret_status = _FactoryReset(object, NULL, NULL, NULL);

    assert_int_equal(ret_status, amxd_status_ok);
    assert_int_equal(access("fb", F_OK), 0);
    assert_int_equal(access("rb", F_OK), 0);

    // We expect the call for reboot to work so the timeout should be ignored
    amxp_timers_enable(false);
    read_sig_alarm();
    amxc_var_clean(&test_args);
}

void test_factory_reset_reboot_via_callback(UNUSED void** state) {
    amxc_var_t test_args;
    amxd_object_t* object = amxd_dm_findf(&dm, "Device.");
    amxd_status_t ret_status = amxd_status_unknown_error;

    assert_int_equal(system("cp mock_reboot /usr/local/bin"), 0);
    assert_int_equal(system("chmod +x /usr/local/bin/mock_reboot"), 0);

    assert_non_null(object);

    amxc_var_init(&test_args);
    amxc_var_set_type(&test_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &test_args, "Cause", "LocalFactoryReset");
    amxc_var_add_key(cstring_t, &test_args, "Reason", "Initiated by Unknown");
    expect_check(__wrap_amxb_call, args, validate_args, &test_args);

    // factory reset = firstboot + reboot
    ret_status = _FactoryReset(object, NULL, NULL, NULL);
    assert_int_equal(access("fb", F_OK), 0);
    assert_int_equal(access("rb", F_OK), 0);
    assert_int_equal(ret_status, amxd_status_ok);

    amxp_timers_enable(true);
    expect_function_call(reboot);
    read_sig_alarm();
    amxc_var_clean(&test_args);
}

void test_get_reboot_entries(UNUSED void** state) {
    amxc_var_t args;
    amxc_var_t test_args;
    amxd_object_t* object = amxd_dm_findf(&dm, "Device.");
    amxd_status_t ret_status = amxd_status_unknown_error;

    assert_int_equal(system("cp mock_reboot /usr/local/bin"), 0);
    assert_int_equal(system("chmod +x /usr/local/bin/mock_reboot"), 0);
    remove("rb");
    remove("fb");

    assert_non_null(object);

    amxc_var_init(&args);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "Cause", "LocalReboot");
    amxc_var_add_key(cstring_t, &args, "Reason", "Initiated by TR069");

    amxc_var_init(&test_args);
    amxc_var_set_type(&test_args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &test_args, "Cause", "LocalReboot");
    amxc_var_add_key(cstring_t, &test_args, "Reason", "Initiated by TR069");

    expect_check(__wrap_amxb_call, args, validate_args, &test_args);

    ret_status = _Reboot(object, NULL, &args, NULL);

    assert_int_not_equal(access("fb", F_OK), 0);
    assert_int_equal(access("rb", F_OK), 0);
    assert_int_equal(ret_status, amxd_status_ok);

    amxp_timers_enable(true);
    expect_function_call(reboot);
    read_sig_alarm();

    amxc_var_clean(&args);
    amxc_var_clean(&test_args);
}
