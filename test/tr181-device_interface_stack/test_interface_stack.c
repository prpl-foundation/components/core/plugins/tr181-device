/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "test_interface_stack.h"
#include "device_interface_stack.h"

static const char* odl_defs = "tr181-device_test.odl";

static amxd_dm_t dm;
static amxo_parser_t parser;
static amxb_bus_ctx_t netmodel_ctx;
static amxp_slot_fn_t slot_cb_link;
static amxp_slot_fn_t slot_cb_unlink;
static amxc_var_t* ret_getLinkInformation = NULL;

amxb_bus_ctx_t* __wrap_amxb_be_who_has(const cstring_t path);
int __wrap_amxb_subscribe(amxb_bus_ctx_t* const ctx,
                          const char* object,
                          const char* expression,
                          amxp_slot_fn_t slot_cb,
                          void* priv);
int __wrap_amxb_call(amxb_bus_ctx_t* const bus_ctx,
                     const char* object,
                     const char* method,
                     amxc_var_t* args,
                     amxc_var_t* ret,
                     int timeout);

int __wrap_amxb_call(UNUSED amxb_bus_ctx_t* const bus_ctx,
                     UNUSED const char* object,
                     const char* method,
                     UNUSED amxc_var_t* args,
                     amxc_var_t* ret,
                     UNUSED int timeout) {
    int ret_val = 0;

    if(strcmp(method, "getLinkInformation") == 0) {
        amxc_var_set_type(ret, AMXC_VAR_ID_LIST);
        ret = amxc_var_add(amxc_llist_t, ret, NULL);
        amxc_var_copy(ret, ret_getLinkInformation);
    } else {
        ret_val = 1;
    }
    return ret_val;
}

amxb_bus_ctx_t* __wrap_amxb_be_who_has(const cstring_t path) {
    amxb_bus_ctx_t* ret_ctx = NULL;
    if(strncmp(path, "NetModel", 8) == 0) {
        ret_ctx = &netmodel_ctx;
    }

    return ret_ctx;
}

int __wrap_amxb_subscribe(UNUSED amxb_bus_ctx_t* const ctx,
                          UNUSED const char* object,
                          const char* expression,
                          amxp_slot_fn_t slot_cb,
                          UNUSED void* priv) {
    int ret = 0;

    if(strcmp(expression, "notification == 'nm:link'") == 0) {
        slot_cb_link = slot_cb;
    } else if(strcmp(expression, "notification == 'nm:unlink'") == 0) {
        slot_cb_unlink = slot_cb;
    } else {
        ret = 1;
    }

    return ret;
}

int test_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    _device_main(0, &dm, &parser);
    return 0;
}

int test_teardown(UNUSED void** state) {
    _device_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

static void add_interface_stack_info_to_htable(amxc_var_t* htable, const char* hAlias, const char* hLayer, const char* lAlias, const char* lLayer) {
    amxc_var_add_key(cstring_t, htable, "HigherAlias", hAlias);
    amxc_var_add_key(cstring_t, htable, "HigherLayer", hLayer);
    amxc_var_add_key(cstring_t, htable, "LowerAlias", lAlias);
    amxc_var_add_key(cstring_t, htable, "LowerLayer", lLayer);
}

static amxd_object_t* find_insterface_stack(const char* hAlias, const char* hLayer, const char* lAlias, const char* lLayer) {
    return amxd_dm_findf(device_get_dm(), "Device.InterfaceStack.[HigherAlias == '%s' && HigherLayer == '%s' && LowerAlias == '%s' && LowerLayer == '%s'].", hAlias, hLayer, lAlias, lLayer);
}

void test_dm_on_startup(UNUSED void** state) {
    amxc_var_t* htable = NULL;
    amxc_var_new(&ret_getLinkInformation);

    amxc_var_set_type(ret_getLinkInformation, AMXC_VAR_ID_LIST);
    htable = amxc_var_add(amxc_htable_t, ret_getLinkInformation, NULL);
    add_interface_stack_info_to_htable(htable, "haStartup0", "hlStartup0", "laStartup0", "llStartup0");
    htable = amxc_var_add(amxc_htable_t, ret_getLinkInformation, NULL);
    add_interface_stack_info_to_htable(htable, "haStartup1", "hlStartup1", "laStartup1", "llStartup1");

    device_subscribe_interface_stack();

    //Check if the interfacestack datamodel has been updated
    assert_non_null(find_insterface_stack("haStartup0", "hlStartup0", "laStartup0", "llStartup0"));
    assert_non_null(find_insterface_stack("haStartup1", "hlStartup1", "laStartup1", "llStartup1"));

    amxc_var_delete(&ret_getLinkInformation);
}

void test_dm_on_link_event(UNUSED void** state) {

    amxc_var_t htable_info;

    amxc_var_init(&htable_info);

    device_subscribe_interface_stack();

    //Check the instances do not exist yet
    assert_null(find_insterface_stack("haEvent0", "hlaEvent0", "laaEvent0", "llaEvent0"));
    assert_null(find_insterface_stack("haaEvent1", "hlaEvent1", "laaEvent1", "llaEvent1"));

    //Create instances with the link event
    amxc_var_set_type(&htable_info, AMXC_VAR_ID_HTABLE);
    add_interface_stack_info_to_htable(&htable_info, "haEvent0", "hlaEvent0", "laaEvent0", "llaEvent0");
    slot_cb_link(NULL, &htable_info, NULL);
    assert_non_null(find_insterface_stack("haEvent0", "hlaEvent0", "laaEvent0", "llaEvent0"));
    amxc_var_clean(&htable_info);

    amxc_var_set_type(&htable_info, AMXC_VAR_ID_HTABLE);
    add_interface_stack_info_to_htable(&htable_info, "haaEvent1", "hlaEvent1", "laaEvent1", "llaEvent1");
    slot_cb_link(NULL, &htable_info, NULL);
    assert_non_null(find_insterface_stack("haaEvent1", "hlaEvent1", "laaEvent1", "llaEvent1"));
    amxc_var_clean(&htable_info);

    //Delete instances with the unlink event
    amxc_var_set_type(&htable_info, AMXC_VAR_ID_HTABLE);
    add_interface_stack_info_to_htable(&htable_info, "haEvent0", "hlaEvent0", "laaEvent0", "llaEvent0");
    slot_cb_unlink(NULL, &htable_info, NULL);
    assert_null(find_insterface_stack("haEvent0", "hlaEvent0", "laaEvent0", "llaEvent0"));
    amxc_var_clean(&htable_info);

    amxc_var_set_type(&htable_info, AMXC_VAR_ID_HTABLE);
    add_interface_stack_info_to_htable(&htable_info, "haaEvent1", "hlaEvent1", "laaEvent1", "llaEvent1");
    slot_cb_unlink(NULL, &htable_info, NULL);
    assert_null(find_insterface_stack("haaEvent1", "hlaEvent1", "laaEvent1", "llaEvent1"));
    amxc_var_clean(&htable_info);
}



